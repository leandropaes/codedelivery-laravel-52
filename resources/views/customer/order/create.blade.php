@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Novo Pedido</h1>
                @include('errors._check')
            </div>
        </div>

        {!! Form::open(['route' => 'customer.order.store']) !!}

        <div class="row">
            <div class="col-md-12">

                <div class="form-group">
                    {!! Form::label('total', 'Total') !!}
                    <p id="total"></p>
                    <a href="#" id="btnNewItem" class="btn btn-primary">Novo Item</a>
                </div>

                <table class="table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>Produto</th>
                        <th width="1">Quantidade</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <select name="items[0][product_id]" class="form-control">
                                @forelse($products as $p)
                                    <option value="{{ $p->id }}" data-price="{{ $p->price }}">{{ $p->name }} --- {{ $p->price }}</option>
                                @empty
                                @endforelse
                            </select>
                        </td>
                        <td>
                            {!! Form::text('items[0][qtd]', 1, ['class' => 'form-control']) !!}
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div class="form-group">
                    {!! Form::submit('Criar Pedido', ['class' => 'btn btn-primary']) !!}
                </div>

            </div>
        </div>

        {!! Form::close() !!}

    </div>

@endsection

@section('post-script')
    <script>
        $(function() {
            $('#btnNewItem').on('click', function() {
                var row = $('table tbody > tr:last'),
                    newRow = row.clone(),
                    length = $('table tbody tr').length;

                newRow.find('td').each(function() {
                    var td = $(this),
                        input = td.find('input,select'),
                        name = input.attr('name');

                    input.attr('name', name.replace((length - 1) + "", length + ""));
                });

                newRow.find('input').val(1);
                newRow.insertAfter(row);

                calculateTotal();

                return false;
            });

            $('body').on('change', 'select', function() {
                calculateTotal();
            });

            $('body').on('blur', 'input[name*=qtd]', function() {
                calculateTotal();
            });

        });

        calculateTotal();

        function calculateTotal() {
            var total = 0,
                trLen = $('table tbody tr').length,
                tr = null, price, qtd;

            for(var i = 0; i < trLen; i++) {
                tr = $('table tbody tr').eq(i);
                price = tr.find(':selected').data('price');
                qtd = tr.find('input').val();
                total += price * qtd;
            }

            $('#total').html(total);
        }
    </script>
@endsection