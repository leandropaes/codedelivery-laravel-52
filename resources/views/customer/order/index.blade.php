@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Pedidos</h1>
                <a href="{{ route('customer.order.create') }}" class="btn btn-primary">Novo Pedido</a>
                <br><br>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover table-striped">
                    <thead>
                    <tr>
                        <th width="1">ID</th>
                        <th>Total</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($orders as $order)
                        <tr>
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->total }}</td>
                            <td>{{ $order->status }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3">Nenhum registro no momento</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

                {!! $orders->render() !!}
            </div>
        </div>

    </div>

@endsection