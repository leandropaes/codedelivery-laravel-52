@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Pedidos <small>\ Pedido #{{ $order->id }} - {{ $order->created_at }}</small></h1>
            </div>
        </div>

        <div class="well well-xs">
            <div class="row">
                <div class="col-sm-2">
                    <b>Total</b><br>
                    R$ {{ $order->total }}
                </div>
                <div class="col-sm-2">
                    <b>Cliente</b><br>
                    {{ $order->client->user->name }}
                </div>
                <div class="col-sm-6">
                    <b>Entregar em</b><br>
                    {{ $order->client->address }} - {{ $order->client->city }} - {{ $order->client->state }}
                </div>
            </div>
        </div>

        {!! Form::model($order, ['route' => ['admin.orders.update', $order->id]]) !!}

        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('status', 'Status') !!}
                            {!! Form::select('status', $list_status, null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('user_deliveryman_id', 'Entregador') !!}
                            {!! Form::select('user_deliveryman_id', $deliveryman, null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
                    <a href="{{ route('admin.orders.index') }}" class="btn btn-default">Voltar</a>
                </div>
            </div>
        </div>

        {!! Form::close() !!}

    </div>
@endsection