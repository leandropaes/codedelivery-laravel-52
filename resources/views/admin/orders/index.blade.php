@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Pedidos</h1>
                <br>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th width="1">ID</th>
                            <th width="100">Total</th>
                            <th width="100">Data</th>
                            <th>Itens</th>
                            <th width="150">Entregador</th>
                            <th width="1">Status</th>
                            <th width="1">Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($orders as $order)
                        <tr>
                            <td>#{{ $order->id }}</td>
                            <td>R$ {{ $order->total }}</td>
                            <td>{{ $order->created_at }}</td>
                            <td>
                                <ul>
                                    @foreach($order->items as $item)
                                    <li>{{ $item->product->name }}</li>
                                    @endforeach
                                </ul>
                            </td>
                            <td>{{ $order->deliveryman->name or '--' }}</td>
                            <td>{{ $order->status }}</td>
                            <td>
                                <a href="{{ route('admin.orders.edit', ['id' => $order->id]) }}" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-pencil"></i></a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="2">Nenhum registro no momento</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>

                {!! $orders->render() !!}
            </div>
        </div>

    </div>

@endsection