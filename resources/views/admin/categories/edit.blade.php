@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Categorias <small>\ Editando Categoria: {{ $category->name }}</small></h1>

                @include('errors._check')
            </div>
        </div>

        {!! Form::model($category, ['route' => ['admin.categories.update', $category->id]]) !!}

        <div class="row">
            <div class="col-md-12">

                @include('admin.categories.partials._form')

            </div>
        </div>

        {!! Form::close() !!}

    </div>

@endsection