@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Produtos <small>\ Novo Produto</small></h1>

                @include('errors._check')
            </div>
        </div>

        {!! Form::open(['route' => ['admin.products.store']]) !!}

        <div class="row">
            <div class="col-md-12">

                @include('admin.products.partials._form')

            </div>
        </div>

        {!! Form::close() !!}

    </div>

@endsection