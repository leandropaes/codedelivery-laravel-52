@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Produtos</h1>
                <a href="{{ route('admin.products.create') }}" class="btn btn-primary">Novo Produto</a>
                <br><br>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th width="1">ID</th>
                            <th>Produto</th>
                            <th>Categoria</th>
                            <th width="1">Preço</th>
                            <th width="70">Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($products as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->category->name }}</td>
                            <td>{{ $product->price }}</td>
                            <td class="text-center">
                                <a href="{{ route('admin.products.edit', $product->id) }}" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-pencil"></i></a>
                                <a href="{{ route('admin.products.destroy', $product->id) }}" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="2">Nenhum registro no momento</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>

                {!! $products->render() !!}
            </div>
        </div>

    </div>

@endsection