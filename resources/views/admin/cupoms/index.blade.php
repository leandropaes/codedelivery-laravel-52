@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Cupoms</h1>
                <a href="{{ route('admin.cupoms.create') }}" class="btn btn-primary">Novo Cupom</a>
                <br><br>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th width="1">ID</th>
                            <th width="1">Código</th>
                            <th>Valor</th>
                            <th width="1">Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($cupoms as $cupom)
                        <tr>
                            <td>{{ $cupom->id }}</td>
                            <td>{{ $cupom->code }}</td>
                            <td>{{ $cupom->value }}</td>
                            <td>
                                <a href="{{ route('admin.cupoms.edit', ['id' => $cupom->id]) }}" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-pencil"></i></a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4">Nenhum registro no momento</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>

                {!! $cupoms->render() !!}
            </div>
        </div>

    </div>

@endsection