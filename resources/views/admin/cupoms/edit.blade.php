@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Cupoms <small>\ Editando Cupom: {{ $cupom->code }}</small></h1>

                @include('errors._check')
            </div>
        </div>

        {!! Form::model($cupom, ['route' => ['admin.cupoms.update', $cupom->id]]) !!}

        <div class="row">
            <div class="col-md-12">

                @include('admin.cupoms.partials._form')

            </div>
        </div>

        {!! Form::close() !!}

    </div>

@endsection