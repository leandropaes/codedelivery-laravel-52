@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Clientes <small>\ Nova Cliente</small></h1>

                @include('errors._check')
            </div>
        </div>

        {!! Form::open(['route' => ['admin.clients.store']]) !!}

        <div class="row">
            <div class="col-md-12">

                @include('admin.clients.partials._form')

            </div>
        </div>

        {!! Form::close() !!}

    </div>

@endsection