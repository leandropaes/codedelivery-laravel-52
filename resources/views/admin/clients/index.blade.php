@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Clientes</h1>
                <a href="{{ route('admin.clients.create') }}" class="btn btn-primary">Novo Cliente</a>
                <br><br>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th width="1">ID</th>
                            <th>Nome</th>
                            <th width="1">Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($clients as $client)
                        <tr>
                            <td>{{ $client->id }}</td>
                            <td>{{ $client->user->name }}</td>
                            <td>
                                <a href="{{ route('admin.clients.edit', $client->id) }}" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-pencil"></i></a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="2">Nenhum registro no momento</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>

                {!! $clients->render() !!}
            </div>
        </div>

    </div>

@endsection