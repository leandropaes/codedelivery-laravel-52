@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Clientes <small>\ Editando Cliente: {{ $client->user->name }}</small></h1>

                @include('errors._check')
            </div>
        </div>

        {!! Form::model($client, ['route' => ['admin.clients.update', $client->id]]) !!}

        <div class="row">
            <div class="col-md-12">

                @include('admin.clients.partials._form')

            </div>
        </div>

        {!! Form::close() !!}

    </div>

@endsection