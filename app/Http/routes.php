<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

// Admin
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth.checkrole:admin']], function() {

    // Categories
    Route::group(['prefix' => 'categories', 'as' => 'categories.'], function() {
        Route::get('', ['uses' => 'CategoriesController@index', 'as' => 'index']);
        Route::get('create', ['uses' => 'CategoriesController@create', 'as' => 'create']);
        Route::get('edit/{id}', ['uses' => 'CategoriesController@edit', 'as' => 'edit']);
        Route::post('store', ['uses' => 'CategoriesController@store', 'as' => 'store']);
        Route::post('update/{id}', ['uses' => 'CategoriesController@update', 'as' => 'update']);
    });

    // Clients
    Route::group(['prefix' => 'clients', 'as' => 'clients.'], function() {
        Route::get('', ['uses' => 'ClientsController@index', 'as' => 'index']);
        Route::get('create', ['uses' => 'ClientsController@create', 'as' => 'create']);
        Route::get('edit/{id}', ['uses' => 'ClientsController@edit', 'as' => 'edit']);
        Route::post('store', ['uses' => 'ClientsController@store', 'as' => 'store']);
        Route::post('update/{id}', ['uses' => 'ClientsController@update', 'as' => 'update']);
    });

    // Products
    Route::group(['prefix' => 'products', 'as' => 'products.'], function() {
        Route::get('', ['uses' => 'ProductsController@index', 'as' => 'index']);
        Route::get('create', ['uses' => 'ProductsController@create', 'as' => 'create']);
        Route::get('edit/{id}', ['uses' => 'ProductsController@edit', 'as' => 'edit']);
        Route::post('store', ['uses' => 'ProductsController@store', 'as' => 'store']);
        Route::post('update/{id}', ['uses' => 'ProductsController@update', 'as' => 'update']);
        Route::get('destroy/{id}', ['uses' => 'ProductsController@destroy', 'as' => 'destroy']);
    });

    // Orders
    Route::group(['prefix' => 'orders', 'as' => 'orders.'], function() {
        Route::get('', ['uses' => 'OrdersController@index', 'as' => 'index']);
        Route::get('{id}', ['uses' => 'OrdersController@edit', 'as' => 'edit']);
        Route::post('update/{id}', ['uses' => 'OrdersController@update', 'as' => 'update']);
    });

    // Cupoms
    Route::group(['prefix' => 'cupoms', 'as' => 'cupoms.'], function() {
        Route::get('', ['uses' => 'CupomsController@index', 'as' => 'index']);
        Route::get('create', ['uses' => 'CupomsController@create', 'as' => 'create']);
        Route::post('store', ['uses' => 'CupomsController@store', 'as' => 'store']);
        Route::get('edit/{id}', ['uses' => 'CupomsController@edit', 'as' => 'edit']);
        Route::post('update/{id}', ['uses' => 'CupomsController@update', 'as' => 'update']);
    });
});

// Customer
Route::group(['prefix' => 'customer', 'as' => 'customer.', 'middleware' => ['auth.checkrole:client']], function() {

    // Checkout
    Route::group(['prefix' => 'order', 'as' => 'order.'], function() {
        Route::get('', ['uses' => 'CheckoutController@index', 'as' => 'index']);
        Route::get('create', ['uses' => 'CheckoutController@create', 'as' => 'create']);
        Route::post('store', ['uses' => 'CheckoutController@store', 'as' => 'store']);
    });

});

// OAuth2
Route::post('oauth/access_token', function() {
    return Response::json(Authorizer::issueAccessToken());
});

// API
Route::group(['prefix' => 'api', 'as' => 'api.', 'middleware' => ['oauth'], 'namespace' => 'Api'], function() {

    Route::get('authenticated', ['as' => 'authenticated', 'uses' => 'AuthenticatedController@index']);

    // Client
    Route::group(['prefix' => 'client', 'as' => 'client.', 'middleware' => ['oauth.checkrole:client'], 'namespace' => 'Client'], function() {
        Route::resource('order', 'ClientCheckoutController', ['except' => ['create', 'edit', 'destroy']]);
    });

    // Deliveryman
    Route::group(['prefix' => 'deliveryman', 'as' => 'deliveryman.', 'middleware' => ['oauth.checkrole:deliveryman'], 'namespace' => 'Deliveryman'], function() {
        Route::resource('order', 'DeliverymanCheckoutController', ['except' => ['create', 'edit', 'destroy', 'store']]);
        Route::patch('order/{id}/update-status', ['as' => 'order.update_status', 'uses' => 'DeliverymanCheckoutController@updateStatus']);
    });

});

