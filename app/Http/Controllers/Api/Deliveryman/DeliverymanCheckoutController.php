<?php

namespace CodeDelivery\Http\Controllers\Api\Deliveryman;

use CodeDelivery\Http\Controllers\Controller;
use CodeDelivery\Repositories\OrderRepository;
use CodeDelivery\Repositories\UserRepository;
use CodeDelivery\Services\OrderService;
use Illuminate\Http\Request;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class DeliverymanCheckoutController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var OrderService
     */
    private $orderService;
    /**
     * @var OrderRepository
     */
    private $repository;

    public function __construct(
        OrderRepository $repository,
        UserRepository $userRepository,
        OrderService $orderService
    )
    {
        $this->repository = $repository;
        $this->userRepository = $userRepository;
        $this->orderService = $orderService;
    }

    public function index()
    {
        $deliverymanId = Authorizer::getResourceOwnerId();
        $orders = $this->repository->with('items.product')->getByDeliveryman($deliverymanId)->paginate();
        return $orders;
    }

    public function show($id)
    {
        $deliverymanId = Authorizer::getResourceOwnerId();
        return $this->repository->getByIdAndDeliveryman($id, $deliverymanId);
    }

    public function updateStatus(Request $request, $id)
    {
        $deliverymanId = Authorizer::getResourceOwnerId();
        $order = $this->orderService->updateStatus($id, $deliverymanId, $request->get('status'));

        if($order) {
            return $order;
        }

        return response()->json(['error' => 'Pedido não encontrado'], 400);
    }
}
