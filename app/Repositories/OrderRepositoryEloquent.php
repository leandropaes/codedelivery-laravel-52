<?php

namespace CodeDelivery\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use CodeDelivery\Models\Order;
use CodeDelivery\Validators\OrderValidator;

/**
 * Class OrderRepositoryEloquent
 * @package namespace CodeDelivery\Repositories;
 */
class OrderRepositoryEloquent extends BaseRepository implements OrderRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Order::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getByClient($clientId)
    {
        return $this->model->newQuery()->where('client_id', $clientId);
    }

    public function getByDeliveryman($deliverymanId)
    {
        return $this->model->newQuery()->where('user_deliveryman_id', $deliverymanId);
    }

    public function getByIdAndDeliveryman($id, $deliverymanId)
    {
        $result = $this->with(['client', 'items.product', 'cupom'])->findWhere([
            'id' => $id,
            'user_deliveryman_id' => $deliverymanId
        ]);
        return $result->first();
    }
}
